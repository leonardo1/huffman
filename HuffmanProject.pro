#-------------------------------------------------
#
# Project created by QtCreator 2015-05-11T13:22:41
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = HuffmanProject
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    reader.h
