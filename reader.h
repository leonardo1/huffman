#ifndef READER_H
#define READER_H
#include <QString>
#include <QFile>
#include <QByteArray>
#include <QDebug>
#include <QIODevice>

class reader{

public:
    int * counter(QString directory){
    QFile * file = new QFile(directory);
        if(file->open(QIODevice::ReadOnly)){
         QByteArray fileFolder = file->readAll();
            for (int i = 0; i < fileFolder.size(); i++){
                occur[uchar(fileFolder.at(i))]++;
            }
        } else{
            return NULL;
          }
       return occur;
}

private:
    int occur[256] = {0};

};

#endif // READER_H
